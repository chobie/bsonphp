<?php
/**
* Bson Encoder/Decoder Class
*
* @author Shuhei Tanuma
* @copyright 2010 Shuhei Tanuma
* @created 19:43 2010/02/06
* @licence Apache License 2.0
*
*   Copyright 2010 Shuhei Tanuma
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*
*/
class Bson{
	public static $instance;


	/**
		data_number
		number:1
		data_format:double
	*/
	const DATA_NUMBER = 1;
	
	/**
		*data_string
		number:2
		data_format:int32 cstring
		The int32 is the # bytes following (# of bytes in string + 1 for terminating NULL)
	*/
	const DATA_STRING = 2;

	/**
		*data_object
		number:3
		data_format:bson_object
	*/
	const DATA_OBJECT = 3;

	/**
		*data_array
		number:4
		data_format:bson_object
		See note on data_array
	*/
	const DATA_ARRAY  = 4;

	/**
		-data_binary
		number:5
		data_format:int32 byte byte[]
		The first int32 is the # of bytes following the byte subtype. Please see note on data_binary
	*/
	const DATA_BINARY = 5;

	/**
		-data_undefined
		number:6
		data_format:VOID
		Conceptually equivalent to Javascript undefined.  Deprecated.
	*/
	const DATA_UNDEFINED = 6;// not support

	/**
			-data_oid
			number:7
			data_format:byte[12]
			12 byte object id.
	*/
	const DATA_OID    = 7;// not support

	/**
		*data_boolean
		number:8
		data_format:byte
		legal values: 0x00 -> false, 0x01 -> true
	*/
	const DATA_BOOLEAN = 8;

	/**
		-data_date
		number:9
		data_format:int64
		value: milliseconds since epoch (e.g. new Date.getTime())
	*/
	const DATA_DATE   = 9;

	/**
		-data_null
		number:10
		data_format:VOID
		Mapped to Null in programming languages which have a Null value or type.
		Conceptually equivalent to Javascript null. 
	*/
	const DATA_NULL   = 10;// not support

	/**
		-data_regex
		number:11
		data_format:cstring cstring
		first ctring is regex expression, second cstring are regex options See note on data_regex
	*/
	const DATA_REGEX   = 11;// not support

	/**
		-data_ref
		number:12
		data_format:int32 cstring byte[12]
		Deprecated.
		Please use a subobject instead -- see page DB Ref. 
			The int32 is the length in bytes of the cstring. 
			The cstring is the namespace: full collection name. 
			The byte array is a 12 byte object id. See note on data_oid.
	*/
	const DATA_REF    = 12;// Deprecated

	/**
		-data_code
		number:13
		data_format:int32 cstring
		The int32 is the # bytes following (# of bytes in string + 1 for terminating NULL) and
		then the code as cstring. data_code should be supported in BSON encoders/decoders,
		but has been deprecated in favor of data_code_w_scope
	*/
	const DATA_CODE  = 13;// not support

	/**
		-data_symbol
		number:14
		data_format:int32 cstring
		same as data_string but for languages with distinct symbol type
	*/
	const DATA_SYMBOL = 14;// not support

	/**
		-data_code_w_scope
		number:15
		data_format:int32 int32 cstring bson_object
		The first int32 is the total # of bytes (size of cstring + size of bson_object + 8 for the two int32s).
		The second int 32 is the size of the cstring (# of bytes in string + 1 for terminating NULL).
		The cstring is the code. The bson_object is an object mapping identifiers to values,
		representing the scope in which the code should be evaluated.
	*/
	const DATA_CODE_W_SCOPE = 15;// not support

	/**
		*data_int
		number:16
		data_format:int32
	*/
	const DATA_INT    = 16;

	/**
		-data_timestamp
		number:17
		data_format:int64
		Special internal type used by MongoDB replication and sharding.
		First 4 are a timestamp, next 4 are an incremented field.
		Saving a zero value for data_timestamp has special semantics. 
	*/
	const DATA_TIMESTAMP = 17;

	/**
		*data_long
		number:18
		data_format:int64
		64 bit integer
	*/
	const DATA_LONG   = 18;

	/**
		-data_min_key
		number:-1
		data_format:VOID
		Special type which compares lower than all other possible BSON element values.
		See Comparing Types and Splitting Shards
	*/
	const DATA_MIN_KEY = -1;// not support

	/**
		-data_max_key
		number:127
		data_format:VOID
		Special type which compares greater than all other possible BSON element values.
		See Comparing Types and Splitting Shards
	*/
	const DATA_MAX_KEY  = 127;// not support

	/**
	* <code>
	* $serialized = Bson::Encode(array("0"=>"test"));
	* var_dump($serialized);
	* </code>
	*/ 
	private function __construct(){
	}

	public static function Encode($object,$recursive_call=false){
		if(!self::$instance){
			self::$instance = new Bson();
		}
		$self = self::$instance;

		$encoded_data = "";
		$null_byte = pack("c",0x00);
		$eoo = pack("c",0x00);

		if(is_array($object) || is_object($object)){
			foreach($object as $name => $data){
				if(is_numeric($data) && !is_string($data)){
					if(is_int($data)){
						$encoded_data .= pack("c",self::DATA_INT) . $name . $null_byte . pack("V",$data);
					}else if(is_long($data)){
						$encoded_data .= pack("c",self::DATA_LONG) . $name . $null_byte . pack("V",$data);
					}else if(is_double($data)){
						$encoded_data .= pack("c",self::DATA_NUMBER) . $name . $null_byte . pack("V",$data);
					}
				}else if(is_string($data)){
					$encoded_data .= pack("c",self::DATA_STRING) . $name . $null_byte . pack("V",strlen($data)+1) . $data . $eoo;
				}else if(is_bool($data)){
					$encoded_data .= pack("c",self::DATA_BOOLEAN) . $name . $null_byte . ($data === true) ? pack("c",0x01) : pack("c",0x00) . $eoo;
				}else if(is_array($data)){
					$encoded_data .= pack("c",self::DATA_ARRAY) . $name . $null_byte . self::Encode($data,true) . $eoo;

				}else if(is_object($data)){
					$encoded_data .= pack("c",self::DATA_OBJECT) . $name . $null_byte . self::Encode($data,true) . $eoo;
				}
			}
			if($recursive_call){
				$size = strlen($encoded_data)+5;
			}else{
				$encoded_data .= $eoo;
				$size = strlen($encoded_data)+4;
			}

			return pack("V",$size) . $encoded_data;
		}else{
			//nothing to do;
			return $object;
		}
	}

	private function parseDobule($data,&$offset){
		$size = unpack("V",substr($data,$offset,8));
		$offset+=8;
		return $size[1];
	}

	private function parseInt($data,&$offset){
		$size = unpack("v",substr($data,$offset,4));
		$offset+=4;
		return $size[1];
	}

	private function parseByte($data,&$offset){
		$size = unpack("c",substr($data,$offset,1));
		$offset+=1;
		return $size[1];
	}
	
	private function readString($data,&$offset,$length){
		$string = substr($data,$offset,$length-1);
		$offset+=$length;
		return $string;
	}
	private function readCString($data,&$offset){
		$string = "";
		$size = strlen($data);
		for( $i=$offset; $i<$size; $i++){
			if(ord($data[$i]) == 0){
				$offset = $i+1;
				break;
			}
			$string .= $data[$i];
		}
		return $string;
	}
	
	private function addKeyValue(&$object,$key,$value,$type=0){
		if($type == 0){
			$object[$key] = $value;
		}else{
			$object->$key = $value;
		}
	}
	
	public static function Decode($object,$is_object=false){
		if(!self::$instance){
			self::$instance = new Bson();
		}
		$self = self::$instance;

		$offset = 0;
		if($is_object){
			$result = new StdClass();
		}else{
			$result = array();
		}

		if(($length = strlen($object)) < 4){
			// too shrot to decode.
			return $object;
		}

		$max_size = $self->parseInt($object,$offset);
		if($max_size != $length){
			//it seems invalid format.
			return $object;
		}
		
		while($offset<$max_size){
			$key = null;
			$data = null;
			$type = $self->parseByte($object,$offset);

			switch($type){
				case self::DATA_LONG:
				case self::DATA_NUMBER:
					$key  = $self->readCString($object,$offset);
					$data = (float)$self->parseInt($object,$offset);

					$self->addKeyValue($result,$key,$data,$is_object);
					break;
				case self::DATA_INT:
					$key  = $self->readCString($object,$offset);
					$data = $self->parseInt($object,$offset);

					$self->addKeyValue($result,$key,$data,$is_object);
					break;
				case self::DATA_STRING:
					$key  = $self->readCString($object,$offset);
					$data_size = $self->parseInt($object,$offset);
					$data = $self->readString($object,$offset,$data_size);
					$self->addKeyValue($result,$key,$data,$is_object);
					//$offset++;
					//var_dump("0x" .sprintf("%02s",dechex($offset)));

					break;
				case self::DATA_ARRAY:
				case self::DATA_OBJECT:
					$key  = $self->readCString($object,$offset);
					$self->addKeyValue($result,$key,self::Decode(substr($object,$offset),($type == self::DATA_ARRAY) ? false : true),$is_object);

					$size = $self->parseInt($object,$offset);
					$offset+=$size;
					break;
				case 0:
					//for terminate code.
					break;
				default:
					throw new Exception("unsupported data type found");
					break 2;

			}
		}

		return $result;
	}
}

if(!function_exists("bson_encode")){
	function bson_encode($data){
		return Bson::Encode($data);
	}
}

if(!function_exists("bson_decode")){
	function bson_decode($data){
		return Bson::Decode($data);
	}
}